/* declarar variables */
const btnCalcular = document.getElementById('btnCalcular');
const foot = document.querySelector('.registros');
let id = 0;
btnCalcular.addEventListener('click', (e) => {
    const calculo = {
        valorAuto: 0,
        pInicial: 0,
        plazos: 0,
        pagoInicial: 0,
        totalFin: 0,
        pagoMensual: 0
    };
    // Obtener los datos de los input
    calculo.valorAuto = document.getElementById('ValorAuto').value * 1;
    calculo.pInicial = document.getElementById('porcentaje').value * 1;
    calculo.plazos = document.getElementById('plazos').value * 1;

    // Hacer los calculos
    calculo.pagoInicial = (calculo.valorAuto * (calculo.pInicial / 100)).toFixed(2);
    calculo.totalFin = (calculo.valorAuto - calculo.pagoInicial).toFixed(2);
    calculo.pagoMensual = (calculo.totalFin / calculo.plazos).toFixed(2);

    // Mostrar los datos
    document.getElementById('pagoInicial').value = calculo.pagoInicial;
    document.getElementById('totalfin').value = calculo.totalFin;
    document.getElementById('pagoMensual').value = calculo.pagoMensual;
    insertarRegistro(calculo);
});

const insertarRegistro = ({valorAuto, pInicial, plazos, pagoInicial, totalFin, pagoMensual}) => {
    let registro = document.createElement('p');
    registro.classList.add('r');
    id++;
    registro.innerHTML = `<span class="r-id">${id}</span> <span class="r-va">Valor Auto: $${valorAuto}</span> <span class="r-po">Porcentaje: ${pInicial}%</span> <span class="r-pl">Plazo: ${plazos}</span> <span class="r-pi">Pago Inicial: $${pagoInicial}</span> <span class="r-to">Total a financiar: $${totalFin}</span> <span class="r-pa">Pago mensual: $${pagoMensual}</span>`;
    foot.appendChild(registro); 
}