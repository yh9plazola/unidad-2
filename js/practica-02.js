const btnCalcular = document.querySelector('.btnCalcular');
const lblIMC = document.querySelector('.imc');
const registros = document.querySelector('.registros');
const lbledad = document.querySelector('.lbl-edad');
const lblhombres = document.querySelector('.lbl-hombres');
const lblmujeres = document.querySelector('.lbl-mujeres');
const img = document.querySelector('.calculadora__img');
const rdbHombre = document.querySelector('.rdbHombre');
const rdbMujer = document.querySelector('.rdbMujer');

let contador = 0;

const agregarRegistro = ({peso, altura, edad, genero, imc}) => {
    let registro = document.createElement('p');
    registro.classList.add('r');
    registro.innerHTML = `<span class="r-id">${contador}</span> <span class="r-peso">Peso: ${peso}</span> <span class="r-altura">Altura: ${altura}</span> <span class="r-imc">IMC: ${imc}</span>`;
    registros.appendChild(registro);
    mostrarResultados(imc, edad, genero);
}

const mostrarResultados = (imc, edad, genero) => {
    lbledad.innerText = edad;
    if(edad >= 10 && edad < 18){
        if(genero == 'Hombre'){
            lblhombres.innerText = "17.686 x peso + 658.2";
            lblmujeres.innerText = 0;
        }
        else {
            lblmujeres.innerText = "13.384 x peso + 692.6";
            lblhombres.innerText = 0;
        }
    }
    else if(edad >= 18 && edad < 30){
        if(genero == 'Hombre'){
            lblhombres.innerText = "15.057 x peso + 692.2";
            lblmujeres.innerText = 0;
        }
        else {
            lblmujeres.innerText = "14.818 x peso + 486.6";
            lblhombres.innerText = 0;
        }
    }
    else if(edad >= 30 && edad < 60){
        if(genero == 'Hombre'){
            lblhombres.innerText = "11.472 x peso + 873.1";
            lblmujeres.innerText = 0;
        }
        else {
            lblmujeres.innerText = "8.126 x peso + 845.6";
            lblhombres.innerText = 0;
        }
    }
    else if(edad >= 60){
        if(genero == 'Hombre'){
            lblhombres.innerText = "11.711 x peso + 587.7";
            lblmujeres.innerText = 0;
        }
        else {
            lblmujeres.innerText = "9.082 x peso + 658.5";
            lblhombres.innerText = 0;
        }
    }

    if(imc < 18.5) {
        img.src = `../img/01.png`;
    }
    else if(imc >= 18.5 && imc < 25) {
        img.src = `../img/02.png`;
    }
    else if(imc >= 25 && imc < 30) {
        img.src = `../img/03.png`;
    }
    else if(imc >= 30 && imc < 35) {
        img.src = `../img/04.png`;
    }
    else if(imc >= 35 && imc < 40) {
        img.src = `../img/05.png`;
    }
    else if(imc >= 40) {
        img.src = `../img/06.png`;
    }
}

btnCalcular.addEventListener('click', (e) => {
    e.preventDefault();
    contador++;
    const registro = {
        peso: 0,
        altura: 0,
        edad: 0,
        genero: 0,
        imc: 0
    };
    registro.peso = document.querySelector('#peso').value;
    registro.altura = document.querySelector('#altura').value;
    registro.edad = document.querySelector('#edad').value;
    if(rdbHombre.checked){
        registro.genero = 'Hombre';
    }
    else {
        registro.genero = 'Mujer';
    }
    registro.imc = (registro.peso / (registro.altura * registro.altura)).toFixed(2);
    lblIMC.innerText = `${registro.imc}`;
    agregarRegistro(registro);
});

