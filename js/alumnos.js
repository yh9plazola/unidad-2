// declara objeto alumno

let alumno = {
    matricula: '2021030101',
    nombre: 'Yohan Alek Plazola Arangure',
    grupo: 'TI-73',
    carrera: 'Tecnologías de la Información',
    foto: 'Foto hipotetica',
};

let alumnos = [
    {
        matricula: 'mosuna@upsin.edu.mx',
        nombre: 'Melissa Osuna Cardenas',
        grupo: '7-3',
        carrera: 'Ing. Tecnologías de la Información',
        foto: '../img/alumnos/melissa.jpg',
    },
    {
        matricula: '2020030714',
        nombre: 'Axel Jovani Ruiz Guerrero',
        grupo: '7-3',
        carrera: 'Ing. Tecnologías de la Información',
        foto: '../img/alumnos/axel.jpg',
    },
    {
        matricula: '2021030008',
        nombre: 'Maria Estrella Landeros Andrade',
        grupo: '7-3',
        carrera: 'Ing. Tecnologías de la Información',
        foto: '../img/alumnos/estrella.jpg',
    },
    {
        matricula: '2021030269',
        nombre: 'Gael Mizraim Salas Salazar',
        grupo: '7-3',
        carrera: 'Ing. Tecnologías de la Información',
        foto: '../img/alumnos/gael.jpg',
    },
    {
        matricula: '2020030321',
        nombre: 'Yair Alejandro Ontiveros Govea',
        grupo: '7-3',
        carrera: 'Ing. Tecnologías de la Información',
        foto: '../img/alumnos/govea.jpg',
    },
    {
        matricula: '2019030880',
    nombre: 'Julio Emiliano Quezada Ramos',
        grupo: '7-3',
        carrera: 'Ing. Tecnologías de la Información',
        foto: '../img/alumnos/julio.jpg',
    },
    {
        matricula: '2021030266',
        nombre: 'Jose Manuel Gonzales Ramirez',
        grupo: '7-3',
        carrera: 'Ing. Tecnologías de la Información',
        foto: '../img/alumnos/manuel.jpg',
    },
    {
        matricula: '2021030136',
        nombre: 'Oscar Alejandro Solis Velarde',
        grupo: '7-3',
        carrera: 'Ing. Tecnologías de la Información',
        foto: '../img/alumnos/oscar.jpg',
    },
    {
        matricula: '2021030262',
        nombre: 'Angel Ernesto Qui Mora',
        grupo: '7-3',
        carrera: 'Ing. Tecnologías de la Información',
        foto: '../img/alumnos/qui.jpg',
    },
]

let lista_alumnos = document.querySelector('section.home#home .lista-alumnos');
let template = document.getElementById('template').content;
let frag = document.createDocumentFragment();

const agregarAlumno = ({matricula, nombre, grupo, carrera, foto}) => {
    template.querySelector('.item .item-img img').src = foto;
    template.querySelector('.item .matricula').innerText = matricula;
    template.querySelector('.item .nombre').innerText = nombre;
    template.querySelector('.item .grupo').innerText = grupo;
    template.querySelector('.item .carrera').innerText = carrera;
    let clone = template.cloneNode(true);
    frag.appendChild(clone);
    lista_alumnos.appendChild(frag);
}

alumnos.forEach(item => {
    agregarAlumno(item);
})

/*
console.log("Matricula: " + alumno.matricula);
console.log("Nombre: " + alumno.nombre);

alumno.nombre = "Mateo Arias Tirado CCNA 7 Le sabe al C";
console.log("Nombre: " + alumno.nombre);

let cuentaBanco = {
    numero: '10001',
    banco: 'Banorte',
    cliente: {
        nombre: 'Jose Lopez',
        fechaNacimiento: '2020-01-01',
        sexo: 'M'
    },
    saldo: '10000',
};

console.log('Nombre: ' + cuentaBanco.cliente.nombre);
console.log('Saldo: ' + cuentaBanco.saldo);


let productos = [
    {
        codigo: '1001',
        descripcion: 'Atún',
        precio: '34',
    }, 
    {
        codigo: '1002',
        descripcion: 'Jabón de polvo',
        precio: '23',
    },
    {
        codigo: '1003',
        descripcion: 'Harina',
        precio: '43',
    },
    {
        codigo: '1004',
        descripcion: 'Pasta dental',
        precio: '78',
    }
];


console.log('La descripción es ' + productos[0].descripcion);

productos.forEach(item => {
    console.log("Código: " + item.codigo);
    console.log("Descripción: " + item.descripcion);
    console.log("Precio: " + item.precio);
});*/