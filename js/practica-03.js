const btnGenerar = document.querySelector('.btn-generar');
const inp_cantidad = document.querySelector('.inp-number');
const combo = document.querySelector('.combo');
const cont_generar = document.querySelector('.container__generar');

const pares = document.querySelector('.pares');
const impares = document.querySelector('.impares');
const simetrico = document.querySelector('.simetrico');
/* Manejo de Arrays */

// declaracion de array con elementos enteros
let arreglo = [4, 89, 30, 10, 34, 89, 10, 5, 8, 28];

// diselar una función que recibe como argumento un arreglo
// de enteros e imprime cada elemento y el tamaño del areglo

const mostrarArreglo = arreglo => {
    let arr = [];
    arreglo.forEach(item => arr.push(item));
    return arr;
}

// función para mostrar el promedio de los elementos
// de array

const mostrarPromedio = arreglo => {
    let prom = 0;
    arreglo.forEach(item => prom += item);
    return prom / arreglo.length;
}

// funcion para mostrar los valores pares de un arreglo

const mostrarValoresPares = arreglo => {
    let nuevo = [];
    arreglo.forEach(item => {if(item % 2 == 0) nuevo.push(item)});
    return nuevo;
}

// funcion para mostrar el valor mayor de los elementos de
// un arreglo

const mostrarMayor = arreglo => {
    let max = -Infinity;
    arreglo.forEach(item => max = (item > max) ? item : max);
    return max;
}

// funcion para llenar con valores aleatorios el arreglo

const generarAleatorios = (arreglo, cantidad) => {
    arreglo = [];
    for(let i = 0; i < cantidad; i++) arreglo[i] = (Math.random() * 10).toFixed(0);
    return arreglo;
}

// funcion que muestra el valor menor y la posicion del arreglo

const mostrarMenor = arreglo => {
    let menor = Infinity;
    arreglo.forEach(item => menor = (item < menor) ? item : menor);
    return `Menor: ${menor}, Índice: ${arreglo.indexOf(menor)}`;
}

// funcion para comprobar si el generador de numeros aleatorios
// es simétrico

const esSimetrico = (arreglo) => {
    let valores = [0, 0];
    arreglo.forEach(item => {
        if(item % 2 == 0) valores[0]++;
        else valores[1]++;
    });
    valores[0] = (valores[0] / inp_cantidad.value * 100).toFixed(0);
    valores[1] = (valores[1] / inp_cantidad.value * 100).toFixed(0);
    pares.innerText = `${valores[0]}%`;
    impares.innerText = `${valores[1]}%`;
    return (Math.abs(valores[0] - valores[1]) <= 20) ? true : false;
}

btnGenerar.addEventListener('click', e => {
    combo.innerText = '';
    let cantidad = inp_cantidad.value;
    if(cantidad >= 2) { 
        arreglo = generarAleatorios(arreglo, cantidad);
        arreglo.forEach(i => {
            let item = document.createElement('option');
            item.value = i;
            item.innerText = `${i}`;
            combo.appendChild(item);
        });
        simetrico.innerText = (esSimetrico(arreglo)) ? 'Es simétrico' : 'No es simétrico';
    }
    else return false;
});